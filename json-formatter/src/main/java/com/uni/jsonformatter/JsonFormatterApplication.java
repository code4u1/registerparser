package com.uni.jsonformatter;

import com.uni.jsonformatter.person.Person;

import org.apache.commons.codec.digest.DigestUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class JsonFormatterApplication {
    private static final Path filePath = Path.of("register.txt");

    private static final List<Person> personList = new ArrayList<>();

    public static void main(String[] args) {
    	RegisterAnalyzer analyzer = new RegisterAnalyzer();
    
        try (var bufferedReader = Files.newBufferedReader(filePath)) {
            String line = null;
            boolean flag = false;
            List<String> personData = new ArrayList<>();
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.equals("[")) {
                    flag = true;
                } else if (line.equals("],")) {
                    flag = false;
                    analyzer.processPersonData(personData);
                    personData.clear();
                }

                if (!line.equals("[") && flag) {
                    personData.add(line);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        JsonFormatter.getJsonOfPeople(analyzer.getPersonList());
    }
}
