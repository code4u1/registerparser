package com.uni.jsonformatter;

public class RegisterAnalyzer {
	
   private final List<Person> personList = new ArrayList<>();
	
   public void processPersonData(List<String> personData) {
        Person person = new Person();

        person.setId(Long.parseLong(personData.get(0).substring(1, 4)));
        person.setName(getName(personData.get(1)));
        person.setPhoneNumber(getPhoneNumber(personData.get(2)));
        person.setEmail(getEmail(personData.get(2)));
        person.setAddress(hashAddress(personData.get(2)));
        person.setGroundsForEntry(getProperty(personData.get(3)));
        person.setFieldOfActivity(getProperty(personData.get(4)));
        person.setSpecialty(getProperty(personData.get(5)));

        personList.add(person);
    }

    public List<Person> getPersonList() {
    	return personList;
    }


    private String getProperty(String line) {
        return line.length() != 3 ? line : "";
    }

    private String hashAddress(String address) {
        if (address.length() != 3) {
            return DigestUtils.sha256Hex(address);
        }
        return "";
    }

    private String getName(String line) {
    	String[] names = line.substring(1, line.lastIndexOf("\"")).split("\\s+");
        names[names.length - 3] = names[names.length - 3].charAt(0) + ".";
        names[names.length - 2] = names[names.length - 2].charAt(0) + ".";
        
        return names[names.length - 3] + " " + names[names.length - 2] + " " + names[names.length - 1];
    }

    private String getPhoneNumber(String line) {
        StringBuilder phoneNumber = new StringBuilder();
        int numbers = 0;
        boolean flag = false;
        for (int i = 0; i < line.length() - 1; i++) {
            String temp = line.substring(i, i + 2);
            if (temp.equals("08")) {
                flag = true;
            }
            if (numbers >= 10) {
                return phoneNumber.toString();
            }
            if (flag && Character.isDigit(line.charAt(i))) {
                phoneNumber.append(line.charAt(i));
                numbers++;
            }
        }
        return phoneNumber.toString();
    }

    private String getEmail(String line) {
        StringBuilder email = new StringBuilder();
        int i = 0;
        if (line.contains(".com")) {
            i = line.indexOf(".com") + 3;
        } else if (line.contains(".bg")) {
            i = line.indexOf(".bg") + 2;
        } else if (line.contains(".info")) {
            i = line.indexOf(".info") + 4;
        }
        while (i > 0 && line.charAt(i) != ' ' && line.charAt(i) != 'n') {
            email.append(line.charAt(i));
            i--;
        }
        return email.reverse().toString();
    }
	
}
